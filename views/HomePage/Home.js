/* Libraries */
import React, {useState, useEffect} from 'react';
import {View, StyleSheet, SafeAreaView, Text, TouchableOpacity, Image} from 'react-native';

// RECOIL STATE
import {recentViewedCitiesState} from '../../atoms/State';
import { useSetRecoilState, useRecoilValue } from 'recoil';

// NEUMORPHISM-ELEMENT
import { NeuView, NeuButton } from 'react-native-neu-element'

// LOCATION-PERMISSION
import * as Location from 'expo-location'

// ICONS IMPORT
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';

// THEME IMPORT
import * as theme from '../../constants/Theme';

// POP-UP-SEARCH-AREA
import HomeModal from './HomeModal';

// SCREEN DIMENSIONS
import useScreenDimensions from '../../hooks/useScreenDimensions';

const BASE_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/weather?'
const WEATHER_API_KEY = '16909a97489bed275d13dbdea4e01f59'

export default function Home() {
    
    const [alert, setAlert] = useState(false)
    const [latitude, setLatitude] = useState('')
    const [longitude, setLongitude] = useState('')
    const [errorMsg, setErrorMsg] = useState(null);
    const [currentWeather, setCurrentWeather] = useState(null)
    const [dataDone,setDataDone] = useState(false)
    const [weatherDone, setWeatherDone] = useState(false)

    // RECOIL GLOBAL STATE FOR SAVE LIST OF RECENT VIEWED LOCATION
    const setSaveCityList = useSetRecoilState(recentViewedCitiesState);
    const saveCityList = useRecoilValue(recentViewedCitiesState);
    
    // CURRENT DATE
    let date = new Date().toDateString();
    let arr = date.split(" ")
    arr.splice(3,1)
    let dateModify = arr.join(", ")

    // RESPONSIVE SIZE
    const screenDimension = useScreenDimensions()
    const width= screenDimension.width;
    const height = screenDimension.height;
    const fontsizeBig = height*0.024
    const fontsizeNormal = height * 0.0205
    const fontsizeMedium = height*0.025
    const paddingBottomTextNormal = height*0.006
    const paddingBottomBigGap = height*0.035
  
    useEffect(()=>{
        getWeather()
    },[longitude, latitude, date])

    useEffect(() => {
        getData()
    }, [dataDone])
  
    useEffect(()=>{
        if(  currentWeather !== null ){
            let nameCity = `${currentWeather.name}, ${currentWeather.sys.country}`
            let tempSave = saveCityList;
            let index = tempSave.findIndex(ele=>ele.city==='currentCity')
            let latCheck = currentWeather.coord.lat;
            let longCheck = currentWeather.coord.lon;

            if(index !== -1){
                
                if(Math.round(latCheck) == Math.round(tempSave[index].lat) && Math.round(longCheck) == Math.round(tempSave[index].long)){
                    let newTemp = tempSave.map((ele,i)=> i===index ? {...ele,cityName:nameCity} :ele);
                    setSaveCityList(newTemp)

                }else{
                    setSaveCityList(tempSave)
                }
                
            }else{
                setSaveCityList(tempSave)
            }

        }
        
    },[weatherDone])
 
    // GET CURRENT LOCATION - PERMISSION INCLUDED - ONLY CALL ONCE 
    const getData = async() =>{
        try{
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            
            // IF CURRENT LOCATION CHANGE -CHANGE IT IN GLOBAL STATE
            let tempSave = saveCityList;
            let index = tempSave.findIndex(ele=>ele.city==='currentCity')
            if(index == -1){
                setSaveCityList([...tempSave,{city:'currentCity',lat:location.coords.latitude,long:location.coords.longitude}])
            }else{
                let latCheck = location.coords.latitude;
                let longCheck = location.coords.longitude;
                if(Math.round(latCheck) !== Math.round(tempSave[index].lat) && Math.round(longCheck) !== Math.round(tempSave[index].long)){
                    let newTemp = tempSave.map((ele,i)=> i=== index ? {city:'currentCity', lat:latCheck, long:longCheck} : {...ele})  
                    setSaveCityList(newTemp);

                }else{
                    setSaveCityList(tempSave)
                }
                
            }
            setLatitude(location.coords.latitude)
            setLongitude(location.coords.longitude)
        
            getWeather(latitude,longitude)
            setDataDone(true)

        } catch(e){
            console.log(e)
        }
        
    }
    
    // FETCH DATA FROM API SOURCE
    const getWeather = async (latitude,longitude) =>{
        try{
            const weatherUrl = `${BASE_WEATHER_URL}lat=${latitude}&lon=${longitude}&units=metric&appid=${WEATHER_API_KEY}`
            
            const response = await fetch(weatherUrl)
            const result = await response.json()

            if(response.ok) {
                setCurrentWeather(result) 
                setWeatherDone(true)           
            } else {
                setErrorMsg(result.message)
            }

        }catch(e){
            console.log(e)
        }
    }
    console.log('save list ===>',saveCityList)
  
    // CALLBACK FUNC TO RECEIVE DATA FROM POP-UP SEARCH COMPONENT
    const receiveData = (newLat,newLong,cityName)=>{  
        setLatitude(newLat);
        setLongitude(newLong);
        getWeather(newLat,newLong)
        if(newLat){
            setAlert(false)
        }
    }

    if (currentWeather == null){
        return(
            <Text style={{marginTop:'50%',marginLeft:'40%'}}>loading....</Text>
        )
    } else {
      
        const icon = currentWeather.weather[0].icon
        const iconUrl = `https://openweathermap.org/img/wn/${icon}@4x.png`
        const typeWeather = currentWeather.weather[0].main 
        let index = weatherConditions.findIndex(ele=>ele.main == typeWeather)
        let weatherInfos = index!==-1 ? weatherConditions[index] : weatherConditions[4]

    return (
        <SafeAreaView style={styles.backgroundStyle}>
            <View style={{flex: 1,padding:width*0.05}}>

                {/* HEADER */}
                <View style={styles.header}>
                    <View style={{flexDirection:'column',alignItems:'flex-start',justifyContent:'flex-start'}}>
                        <View style={{flexDirection:'row',flexWrap:'wrap'}}>
                            <Text style={{fontWeight: 'bold', fontSize:fontsizeNormal,textTransform:'uppercase'}}>{currentWeather.name}, </Text>
                            <Text style={{fontWeight: 'bold', fontSize:fontsizeNormal,textTransform:'uppercase'}}>{currentWeather.sys.country}</Text>

                        </View>
                        
                        <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:paddingBottomTextNormal}}>{dateModify}</Text>
                    </View>

                    <NeuButton onPress={()=>setAlert(true)} color={theme.colors.bgCol} height={width*0.14} width={width*0.14} borderRadius={width*0.09}>
                        <Ionicons name={"search-sharp"} size={width*0.07} color='#000' style={{marginRight: -5}} />
                    </NeuButton>
                </View>

                {/* SECTION 1 */}
                <View style={{ alignSelf: 'center', marginTop: '2%' }}>
                    <NeuView color={theme.colors.bgCol} height={height*0.27} width={height*0.27} borderRadius={height*0.18} inset>
                       
                        {weatherInfos.feather ? 
                        <Feather size={height*0.2} color={ theme.colors.themeCol1 } name={weatherInfos.icon} />:
                        weatherInfos.icon==='false' ?
                        <Image style={{width:height*0.21,height:height*0.21,tintColor: weatherInfos.tintColor}} source={{uri: iconUrl}} />:
                        weatherInfos.icon === 'image' ?
                        <Image style={{height:height*0.2,width:height*0.2}} source={weatherInfos.source} /> 
                        : <Ionicons color={ weatherInfos.tintColor} size={height*0.17} name={weatherInfos.icon} />
                        }  
                     
                    </NeuView>
                </View>
                
                {/* SECTION2 */}
                <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',marginTop:paddingBottomBigGap*1.2,borderBottomColor:'#E8ECEF',borderBottomWidth:5,paddingBottom:paddingBottomBigGap}}>
                    <View>
                        <Text style={{color:'#c4c9ce',fontSize:fontsizeMedium,padding:paddingBottomTextNormal}}>Temp</Text>
                        <Text style={{fontSize:fontsizeMedium,padding:paddingBottomTextNormal,fontWeight:'600',textAlign:'center'}}>{Math.round(currentWeather.main.temp)}º</Text>
                    </View>
                    <View style={{borderLeftColor:'#DCE0E4',borderLeftWidth:2,borderRightColor:'#DCE0E4',borderRightWidth:2,paddingLeft:25,paddingRight:25}}>
                        <Text style={{color:'#c4c9ce',fontSize:fontsizeMedium,padding:paddingBottomTextNormal}}>Wind</Text>
                        <Text style={{fontSize:fontsizeMedium,padding:paddingBottomTextNormal,fontWeight:'600',textAlign:'center'}}>{currentWeather.wind.deg}</Text>
                    </View>
                    <View>
                        <Text style={{color:'#c4c9ce',fontSize:fontsizeMedium,padding:paddingBottomTextNormal}}>Humidity</Text>
                        <Text style={{fontSize:fontsizeMedium,padding:paddingBottomTextNormal,fontWeight:'600',textAlign:'center'}}>{currentWeather.main.humidity}%</Text>
                    </View>
                </View>

                {/* SECTION3 */}
                <View style={{marginTop:height*0.045}}>
                    <NeuView color='#F0EFF4' height={height*0.227} width={width*0.9} borderRadius={16}>
                        <View style={styles.viewRow}>
                            <View style={{flex:2,paddingLeft:height*0.017}}>
                                <Text style={{color:'#969B9F',fontSize:fontsizeBig,fontWeight:'bold',paddingBottom:paddingBottomTextNormal,paddingTop:paddingBottomTextNormal}}>Hello, Sweety!</Text>
                                <View style={{flexDirection:'row',alignItems:'baseline',flexWrap:'wrap'}}>
                                    <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingBottom:paddingBottomBigGap}}>{weatherInfos.descrip1}</Text>
                                    <Text style={{fontWeight:'bold',color:'#969B9F',fontSize:height*0.02,paddingLeft:5,paddingBottom:paddingBottomBigGap,textTransform:'capitalize'}}>{index !==-1 ? weatherInfos.descrip2 : currentWeather.weather[0].description}</Text>
                                    <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingLeft:5,paddingBottom:paddingBottomBigGap}}>{weatherInfos.descrip3}</Text>
                                </View>
                                <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingBottom:paddingBottomTextNormal}}>{weatherInfos.advise1}</Text>
                                <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingBottom:paddingBottomTextNormal}}>{weatherInfos.advise2}</Text>
                            </View>
                        
                            <Image style={{borderRadius:16,width:width*0.3,height:height*0.227,flex:1}} source={weatherInfos.images} />

                        </View>
                        
                    
                    </NeuView>
                </View>

                {/* POP-UP-SEARCH AREA */}

                {alert && <HomeModal width={width} height={height} currentWeather={currentWeather} receiveData={receiveData}/>}

                
                    
            </View>
            
        </SafeAreaView>
    );
}}

const styles = StyleSheet.create({
    backgroundStyle:{
        flex:1,
        backgroundColor:theme.colors.bgCol
    },
    header:{
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    viewRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
});



// MODIFY ADVISES AND WEATHER ICONS
const weatherConditions = [
    {
        main:'Thunderstorm',
        descrip1:'There is thunder-storm',
        descrip2:'thunder-storm',
        descrip3:'today.',
        advise1:'Stay safe at home with a warm coffee could be the best!',
        advise2:'Take care of yourself!',
        images:require('../../photos/stormy.png'),
        feather: false,
        icon:"thunderstorm-outline",
        tintColor:'#B7ACEE'
    },
    {
        main:'Drizzle',
        descrip1:'There is a',
        descrip2:'drizzle',
        descrip3:'today.',
        advise1:"Don't forget to bring an umbrella or rain coat",
        advise2:'Take care of yourself!',
        images:require('../../photos/RainyDay2.png'),
        feather: true,
        icon:"cloud-drizzle",
        tintColor:'#B7ACEE'

    },
    {
        main:'Rain',
        descrip1:'Today is a',
        descrip2:'rainy',
        descrip3:'day.',
        advise1:"Don't forget to bring an umbrella or rain coat",
        advise2:'Take care of yourself!',
        images:require('../../photos/rainy.png'),
        feather: false,
        icon:"rainy-outline",
        tintColor:'#B7ACEE'
    },
    {
        main:'Snow',
        descrip1:'Today is a',
        descrip2:'snowy ',
        descrip3:'day.',
        advise1:"Keep your body warm from cold weather",
        advise2:'Take care of yourself!',
        images:require('../../photos/RainyDay.png'),
        feather: false,
        icon:"snow-outline"
    },
    {
        main:'Atmosphere',
        descrip1:'There is',
        advise1:"Stay alerted!",
        advise2:'Take care of yourself!',
        images:require('../../photos/rainy.png'),
        feather: false,
        icon:'false'
    },
    {
        main:'Clear',
        descrip1:'It is an amazing',
        descrip2:'sunny',
        descrip3:'day',
        advise1:"Going on a picnic or to the beach could be the best!",
        advise2:'Enjoy your day!',
        images:require('../../photos/sunnyBeachAdvise.png'),
        feather: false,
        icon:'image',
        source:require('../../photos/sunGIF.gif'),
        tintColor:'#ECD166'
    },
    {
        main:'Clouds',
        descrip1:'Today is a',
        descrip2:'cloudy',
        descrip3:'day.',
        advise1:"Joining outdoor activities with friends could be fun!",
        advise2:'Stay tunned!',
        images:require('../../photos/outdoor2.png'),
        feather: false,
        icon:"cloudy-outline",
        tintColor:'#389ACE'
    }
]
