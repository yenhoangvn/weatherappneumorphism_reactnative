/* Libraries */
import React, {useState, useEffect} from 'react';
import {View, StyleSheet, SafeAreaView, Text, TouchableOpacity, Image, FlatList, List, ActivityIndicator} from 'react-native';
import axios from 'axios';


// RECOIL STATE
import {recentViewedCitiesState} from '../../atoms/State';
import { useSetRecoilState, useRecoilValue } from 'recoil';


// NEUMORPHISM-ELEMENT
import { NeuView, NeuButton } from 'react-native-neu-element'
import { NeuInput } from 'react-native-neu-element';

// THEME IMPORT
import * as theme from '../../constants/Theme';

// ICONS
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


 export default function AirModal({ receiveData, width, height, airQuality }){

    const [searchKeyWord,setSearchKeyWord] = useState();
    const [searchResult, setSearchResult] = useState([])
    const [isShowResult,setIsShowingResult] = useState(false);
    const [alertPrompt,setAlertPrompt] = useState(false)

    const API_KEY = 'AIzaSyBqBD5MpX8-sW5M9cLJ11DG62NsrH0Eo7w'

    // RECOIL GLOBAL STATE
    const setSaveCityList = useSetRecoilState(recentViewedCitiesState);
    const saveCityList = useRecoilValue(recentViewedCitiesState);
    
    // useEffect(()=>{
    // },[searchKeyWord])

    // CUSTOME GOOGLE AUTOCOMPLETE INPUT AREA - SHOWING PREDICTION PLACES
    const searchLocation = async (text) => {
        setSearchKeyWord(text);
        try{
            let url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${API_KEY}&input=${searchKeyWord}`
            const response = await axios.request({
                method:'post',
                url:url
            })
           
            setIsShowingResult(true)
            setSearchResult(response.data.predictions);
            
        }catch (e){
            console.log(e)
        }
    };
    // MAKING API CALL TO FETCH DATA OF LOCATION FROM PLACE_ID 
    const newLocation = async(place_id)=>{
        try{
            let url2 = `https://maps.googleapis.com/maps/api/geocode/json?place_id=${place_id}&key=${API_KEY}`
            const res = await axios.request({
                method:'get',
                url: url2
            })
            let newLat = res.data.results[0].geometry.location.lat
            let newLong = res.data.results[0].geometry.location.lng
            let cityName = res.data.results[0].formatted_address
            
            receiveData(newLat,newLong,cityName);
            let temp = saveCityList;
            let index = temp.findIndex(ele=>ele.city===cityName)
    
            if(index == -1){
                setSaveCityList([...temp,{city:cityName,lat:newLat,long:newLong}])
            }else{
                setSaveCityList(temp)
            }
          
        }catch(e){
            console.log(e)
        }
    }
    
    // FUNCTION TO DELETE THE SAVED CITY
    const removeLocation=(item,index)=>{
        if(index === 0){
            setAlertPrompt(true)
            let temp = saveCityList;
            setSaveCityList(temp);
        }else{
            let temp = saveCityList
            let newTemp = saveCityList.filter((ele,i)=> i !== index);
            setSaveCityList(newTemp);
        }  
    }

    return(
        <SafeAreaView style={{flex: 1, backgroundColor: theme.colors.bgCol,position:'absolute',top:0,left:0,height:height,width:width}}>
            <View style={{padding:25,flex:1}}>
                <View style={{marginBottom:7}}>
                    <NeuInput borderRadius={10} color='#F0EFF4' height={height*0.058} width={width*0.88} onChangeText={(text)=>searchLocation(text)} value={searchKeyWord} placeholder='Search here!'/>
                </View>
                {/* CUSTOM GOOGLE INPUT AUTOCOMPLETE  */}
                {isShowResult && 
                    <NeuView color={theme.colors.bgCol} height={height*0.35} width={width*0.88} borderRadius={5}>
                        <SafeAreaView style={{flex:1}}>
                        <FlatList
                        style={{paddingVertical:0}}
                        data={searchResult}
                        renderItem={({item, index}) => {
                          return (
                              <View style={{paddingHorizontal:10,paddingVertical:10,borderBottomWidth:1,borderColor:'#ddd'}}>
                                  <TouchableOpacity
                                    onPress={(e) => {
                                    e.preventDefault()
                                    setSearchKeyWord(item.description);
                                    newLocation(item.place_id);
                                    }}>                         
                                    <Text style={{padding:2,color:'#A2A2A2',fontSize:height*0.021}}>{item.description}</Text>  
                                </TouchableOpacity>

                              </View>          
                          );
                        }}
                        keyExtractor={(item,index) => `${item.description}${index}`} 
                        >
                        </FlatList>
                        </SafeAreaView>
                    </NeuView>
                }
                
                <View style={{marginTop:width*0.085,marginBottom:width*0.085,flexDirection:'column',alignItems:'stretch',width:width*0.88}}>
                    <NeuView color={theme.colors.bgCol} height={height*0.32} width={width*0.88} borderRadius={16}>
                        {/* CURRENT LOCATION */}
                        
                        <View style={{flexDirection:'row',marginBottom:width*0.05,marginTop:width*0.04, alignItems:'center',marginLeft:width*0.135,width:width*0.88,justifyContent:'flex-start'}}>
                           <View>
                           <NeuView color={theme.colors.bgCol} height={width*0.11} width={width*0.11} borderRadius={width*0.02}>
                                <Ionicons name="navigate" size={width*0.05} color='#9D999A'/>
                            </NeuView>

                           </View>


                            <View style={{flexDirection:'column',marginLeft:30}}>
                                <TouchableOpacity
                                onPress={()=>{
                                    receiveData(saveCityList[0].lat,saveCityList[0].long,saveCityList[0].cityName);   
                                }}
                                >
                                    <Text adjustsFontSizeToFit={true} style={{fontWeight:'bold',fontSize:17,textAlign:'left',marginBottom:5,fontSize:height*0.026}}>Current Location</Text>
                                    <Text adjustsFontSizeToFit={true} style={{color:'#A2A2A2',textAlign:'left',fontSize:height*0.021}}>{saveCityList[0].cityName} - AQI: {airQuality.list[0].main.aqi}</Text>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                        
                        {/* RECENT VIEWED */}
                        <NeuView color='#9EC7D5' height={height*0.04} width={width*0.88} inset>
                            <Text adjustsFontSizeToFit={true} style={{color:'#fff',fontWeight:'bold',fontSize:height*0.021,width:width*0.88,marginLeft:width*0.14}}>RECENTLY VIEWED</Text>
                        </NeuView>
                        {alertPrompt && <Text style={{color:'red',paddingTop:10}}>You can not delete your current location!!</Text>}
                        
                        <FlatList
                        style={{marginLeft:width*0.03}}
                        data={saveCityList}
                        renderItem={({item, index}) => {
                          return (
                            <View style={{paddingHorizontal:10,paddingVertical:10,borderBottomWidth:1,borderColor:'#ddd',flexDirection:'row',alignItems:'center',justifyContent:'space-between',width:width*0.83}}>
                                <TouchableOpacity
                                onPress={(e) => {
                                    receiveData(item.lat,item.long,item.city);
                                }}>                        
                                    <Text style={{padding:2,color:'#A2A2A2',fontSize:height*0.021}}>{item.city==='currentCity' ? item.cityName : item.city}</Text> 

                                </TouchableOpacity>
                                <TouchableOpacity
                                onPress={()=>removeLocation(item,index)}
                                >
                                    <Ionicons  name="heart-dislike-outline" size={20} color= '#A2A2A2'/>
                                
                                </TouchableOpacity>

                            </View>
                            
                          );
                        }}
                        keyExtractor={(item,index) => `${item.city}${index}`} 

                        >
                        </FlatList>
                      
                </NeuView>
                </View>

            </View>
       </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    myStarStyle: {
      color: 'yellow',
      backgroundColor: 'transparent',
      textShadowColor: 'black',
      textShadowOffset: {width: 1, height: 1},
      textShadowRadius: 2,
    },
    myEmptyStarStyle: {
      color: 'white',
    }
  });