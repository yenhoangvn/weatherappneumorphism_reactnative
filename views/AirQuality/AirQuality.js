import React, {useState, useEffect} from "react";
import { StyleSheet, Text, View, SafeAreaView } from "react-native";

// NEUMORPHISM-ELEMENT
import { NeuView, NeuButton } from 'react-native-neu-element'

// THEME IMPORT
import * as theme from '../../constants/Theme'

// ICONS IMPORT
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';

// RECOIL STATE
import {recentViewedCitiesState} from '../../atoms/State';
import { useSetRecoilState, useRecoilValue } from 'recoil';

// SCREEN DIMENSIONS
import useScreenDimensions from "../../hooks/useScreenDimensions";


import ProgressCircle from 'react-native-progress-circle'


import AirModal from "./AirModal";

// const FORECAST_URL =  'https://api.openweathermap.org/data/2.5/onecall?'
const WEATHER_API_KEY = '16909a97489bed275d13dbdea4e01f59'
const AIRQUALITY_URL = 'http://api.openweathermap.org/data/2.5/air_pollution?'

export default function AirQuality(){
    // WEATHER INFOS
    // const [forecast, setForecast] = useState(null)
    const [latitude, setLatitude] = useState('')
    const [longitude, setLongitude] = useState('')
    const [alert, setAlert] = useState(false)
    const [currentCity, setCurrentCity] = useState('');
    const [dataDone, setDataDone] = useState(false)
    const[airQuality, setAirQuality] = useState(null)
    const quality = ['Good','Fair','Moderate','Poor','Very Poor']
    const color=['#6CDD10','#F6F732','#F07A30','#ED462F','#7B2123']
    const icon = ['happy-outline','happy-outline','sad-outline','alert-circle-outline','alert-circle-outline']

    // DATE
    let date = new Date().toDateString();
    let arr = date.split(" ")
    arr.splice(3,1)
    let dateModify = arr.join(", ")

    // SCREEN DIMENSIONS
    const screenDimension = useScreenDimensions()
    const width= screenDimension.width;
    const height = screenDimension.height;
    const fontsizeBig = height*0.024
    const fontsizeNormal = height * 0.0205
    const fontsizeMedium = height*0.025
    const paddingBottomTextNormal = height*0.006
    const paddingBottomBigGap = height*0.035
    console.log('screend height ===>',height)

    // RECOIL STATE
    const setSaveCityList = useSetRecoilState(recentViewedCitiesState);
    const saveCityList = useRecoilValue(recentViewedCitiesState);


    useEffect(() => {
        getData()
      }, [dataDone])
  
      useEffect(()=>{
          // getWeather()
        },[longitude, latitude,date])

    // FETCH DATA OF CURRENT LOCATION FROM GLOBAL STATE OF RECOIL
    const getData = () =>{
        let tempSave = saveCityList;
        let index = tempSave.findIndex(ele=>ele.city==='currentCity')
        setLongitude(tempSave[index].long)
        setLatitude(tempSave[index].lat)
        getWeather(latitude,longitude);
        setCurrentCity(tempSave[index].cityName)
        setDataDone(true)
    }

    // FETCH AIR POLLUTION FROM API SOURCE
    const getWeather = async (latitude,longitude) =>{
        try{ 

            const airqualityUrl = `${AIRQUALITY_URL}lat=${latitude}&lon=${longitude}&appid=${WEATHER_API_KEY}`
            const response3 = await fetch(airqualityUrl)
            const result3 = await response3.json()
       
            if(response3.ok){
                setAirQuality(result3)
                
            }
            console.log(result3)
  
        }catch(e){
            console.log(e)
        }
    }
    
    // CALL BACK FUNC TO GET DATA FROM CHILD COMPONENT AIRMODAL
    const receiveData = (newLat,newLong,cityName)=>{
  
        setLatitude(newLat);
        setLongitude(newLong);
        getWeather(newLat,newLong)
        setCurrentCity(cityName)
        if(newLat){
            setAlert(false)
        }
      }

    if(airQuality == null){
        return(
            <Text style={{marginTop:'50%',marginLeft:'40%'}}>loading....</Text>
        )
    }
    return(
        <SafeAreaView style={styles.backgroundStyle}>
            <View style={{flex: 1,padding:width*0.05}}>

                {/* HEADER */}
                <View style={styles.header}>
                    <View style={{flexDirection:'column',alignItems:'flex-start',justifyContent:'flex-start'}}>
                        <Text style={{fontWeight: 'bold', fontSize:fontsizeNormal,textTransform:'uppercase'}}>{currentCity}</Text>
                        <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:paddingBottomTextNormal}}>{dateModify}</Text>
                    </View>

                    <NeuButton onPress={()=>setAlert(true)} color={theme.colors.bgCol} height={width*0.14} width={width*0.14} borderRadius={width*0.09}>
                        <Ionicons name={"search-sharp"} size={width*0.07} color='#000' style={{marginRight: -5}} />
                    </NeuButton>
                </View>

                {/* CIRCULAR INDICATOR PROGRESS SECTION - AQI(AIR QUALITY INDEX) */}
                <View style={{position:'relative',alignSelf:'center',marginTop:paddingBottomBigGap*0.5}}>

                    <ProgressCircle
                    percent={airQuality.list[0].main.aqi*100/5}
                    radius={height*0.165}
                    borderWidth={20}
                    color={color[airQuality.list[0].main.aqi-1]}
                    shadowColor="#e6e5eb" 
                    bgColor="#EEF0F5"
                    >
                        <Text style={{ fontSize: 18 }}>{1}</Text>
                    </ProgressCircle>
                     <View style={{position:'absolute',top:'8.8%',left:'6.4%'}}>
                        <NeuView width={height*0.276} height={height*0.276} borderRadius={height*0.18} color='#EEF0F5'>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{color:'#979CAB',fontSize:fontsizeBig*2}}>AQI = </Text>
                                <Text style={{color:'#979CAB',fontSize:fontsizeBig*2}}>{airQuality.list[0].main.aqi}</Text>
                            </View>

                            
                        </NeuView>
                    </View>
                        


                </View>
                         

                <View style={{paddingBottom:paddingBottomBigGap*1.1 ,borderBottomWidth:2, borderColor:'#ddd' ,flexDirection:'row',alignItems:'baseline',justifyContent:'center'}}>
                    <Ionicons name={icon[airQuality.list[0].main.aqi-1]} color={color[airQuality.list[0].main.aqi-1]} size={fontsizeBig*2} />
                    <Text style={{paddingLeft:10 ,fontWeight:'500',fontSize:fontsizeBig*2,color:color[airQuality.list[0].main.aqi-1],paddingTop:20}}>{quality[airQuality.list[0].main.aqi-1]}</Text>
                    
                    
                </View>
                
                <View style={{paddingTop:paddingBottomBigGap*1.3}}>
                    <NeuView width={width*0.9} height={height*0.2} color={theme.colors.bgCol} borderRadius={height*0.03}>
                        <View style={{width:width*0.7, borderBottomWidth:2, borderColor:'#ddd'}}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontWeight:'bold',fontSize:fontsizeMedium*1.2}}>NO2</Text>
                                    <Text style={{paddingLeft:5,fontSize:fontsizeNormal*0.9,color:'#A7A6AC'}}>μg/m3</Text>
                                </View>
                                <Text style={{paddingBottom:paddingBottomBigGap*0.5,fontWeight:'bold',fontSize:fontsizeMedium*1.2}}>{airQuality.list[0].components.no2}</Text>

                            </View>
                           
                            {/* <Progress.Bar color={color[0]} progress={airQuality.list[0].components.no2/100} width={width*0.7} />  */}

                        </View>

                        <View style={{width:width*0.7,paddingTop:paddingBottomBigGap}}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontWeight:'bold',fontSize:fontsizeMedium*1.2}}>PM2.5</Text>
                                    <Text style={{paddingLeft:5,fontSize:fontsizeNormal*0.9,color:'#A7A6AC'}}>μg/m3</Text>
                                </View>
                                <Text style={{paddingBottom:paddingBottomBigGap*0.5,fontWeight:'bold',fontSize:fontsizeMedium*1.2}}>{airQuality.list[0].components.pm2_5}</Text>

                            </View>
                            
                            {/* <Progress.Bar color={color[3]} progress={airQuality.list[0].components.pm2_5/9} width={width*0.7} /> */}

                        </View>


                    

                    </NeuView>

                </View>
               



            {alert && <AirModal width={width} height={height} receiveData={receiveData} airQuality={airQuality} />}
                
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    backgroundStyle:{
        flex:1,
        backgroundColor:theme.colors.bgCol
    },
    header:{
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    viewRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
});