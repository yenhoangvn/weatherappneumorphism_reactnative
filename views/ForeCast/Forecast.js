/* Libraries */
import React, {useEffect,useState} from 'react';
import {View, StyleSheet, SafeAreaView, Text, TouchableOpacity,FlatList,Image} from 'react-native';

// NEUMORPHISM-ELEMENT
import { NeuView, NeuButton } from 'react-native-neu-element';

// ICONS IMPORT
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';

// THEME IMPORT
import * as theme from '../../constants/Theme';

// RECOIL STATE
import {recentViewedCitiesState} from '../../atoms/State';
import { useSetRecoilState, useRecoilValue } from 'recoil';

// POP-UP-SEARCH-AREA
import ForecastModal from './ForecastModal'; 

// MOMENT
import moment from 'moment';

// SCREEN DIMENSIONS
import useScreenDimensions from '../../hooks/useScreenDimensions';

// API SOURCE URL
const FORECAST_URL =  'https://api.openweathermap.org/data/2.5/onecall?'
const WEATHER_API_KEY = '16909a97489bed275d13dbdea4e01f59'


export default function Forecast() {

    const [alert, setAlert] = useState(false)
    const [forecast, setForecast] = useState(null)
    const [latitude, setLatitude] = useState('')
    const [longitude, setLongitude] = useState('')
    const [errorMsg, setErrorMsg] = useState(null);
    const [currentCity, setCurrentCity] = useState('');
    const [forecastHourly, setForecastHourly] = useState([])
    const [forecastDaily, setForecastDaily] = useState([])
    const [chosedWeather, setChosedWeather] = useState(null)
    const [temp,setTemp] = useState({})
    const [dataDone, setDataDone] = useState(false)
     
    // RECOIL GLOBAL STATE
    const saveCityList = useRecoilValue(recentViewedCitiesState);
    
    let date = new Date().toDateString();
    let arr = date.split(" ")
    arr.splice(3,1)
    let dateModify = arr.join(", ")

    const screenDimension = useScreenDimensions()
    const width= screenDimension.width;
    const height = screenDimension.height;
    const fontsizeNormal = height * 0.0205
    const fontsizeMedium = height*0.025
    const paddingBottomTextNormal = height*0.006
    const paddingBottomBigGap = height*0.035

    useEffect(() => {
      getData()
    }, [dataDone])

    useEffect(()=>{
    },[longitude, latitude,date])
     
    // FETCH CURRENT LOCATION FROM RECOIL
    const getData = () =>{
        let tempSave = saveCityList;
        let index = tempSave.findIndex(ele=>ele.city==='currentCity')
        setLongitude(tempSave[index].long)
        setLatitude(tempSave[index].lat)
        getWeather(latitude,longitude);
        setCurrentCity(tempSave[index].cityName)
        setDataDone(true)
    }
    
    // FETCH FORECAST WEATHER DATA FROM API SOURCE
    const getWeather = async (latitude,longitude) =>{
        try{ 
            const forecastUrl = `${FORECAST_URL}lat=${latitude}&lon=${longitude}&units=metric&appid=${WEATHER_API_KEY}`
            const response2 = await fetch(forecastUrl)
            const result2 = await response2.json()
            
            if(response2.ok){
                setForecast(result2)
                setForecastHourly(result2.hourly);
                setForecastDaily(result2.daily)
                setChosedWeather(result2.current)
               
                setTemp({
                    temp:Math.round(result2.current.temp),
                    max: Math.round(result2.current.temp+1),
                    min: Math.round(result2.current.temp-1)
                })
            }
  
        }catch(e){
            console.log(e)
        }
    }
    
    // CALLBACK FUNC TO RECEIVE DATA FROM SEARCH MODAL
    const receiveData = (newLat,newLong,cityName)=>{
        setLatitude(newLat);
        setLongitude(newLong);
        getWeather(newLat,newLong)
        setCurrentCity(cityName)
        if(newLat){
            setAlert(false)
        }
      }
    
    // EVENT HANDLER ONPRESS TO SET TYPE OF CHOSED WEATHER FORCAST
    const onChangePress = (item,index) =>{
        let tempForecastDaily = forecastDaily
        let newTemp = tempForecastDaily.map((ele)=> ele.style ? {...ele, style:false} : {...ele}) 
        setForecastDaily(newTemp)

        let tempForecast = forecastHourly;
        let newTempForecast = tempForecast.map((ele,i)=> i === index ? {...ele,style:true} : {...ele,style:false});
        setForecastHourly(newTempForecast)
        setChosedWeather(item)
        setTemp({
            temp:Math.round(item.temp),
            max: Math.round(item.temp+1),
            min: Math.round(item.temp-1)
        })
    }

    const onChangePressDaily = (item,index) =>{

        let tempForecast = forecastHourly;
        let newTempForecast = tempForecast.map((ele,i)=> ele.style ? {...ele,style:false} : {...ele});
        setForecastHourly(newTempForecast)

        let tempForecastDaily = forecastDaily
        let newTemp = tempForecastDaily.map((ele,i)=>i===index ? {...ele,style:true} : {...ele,style:false}) 
        setForecastDaily(newTemp)
        setChosedWeather(item)
        setTemp({
            temp:Math.round(item.temp.day),
            max: Math.round(item.temp.max),
            min: Math.round(item.temp.min)
        })
    }
    
    if(chosedWeather == null || forecast == null){
        return(
            <Text style={{marginTop:'50%',marginLeft:'40%'}}>loading....</Text>
         
        )
    }else{
  
        const typeWeather = chosedWeather.weather[0].main 
        let index = weatherConditions.findIndex(ele=>ele.main == typeWeather)
        let weatherInfos = index!==-1 ? weatherConditions[index] : weatherConditions[4]
    
    return (
    <SafeAreaView style={{flex: 1, backgroundColor: theme.colors.bgCol}}>
        <View style={{flex: 1,padding:width*0.05}}>
            {/* HEADER */}
             <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <View style={{flexDirection:'column',alignItems:'flex-start',justifyContent:'flex-start'}}>
                    <Text style={{fontWeight: 'bold', fontSize:fontsizeNormal,textTransform:'uppercase'}}>{currentCity}</Text>
                    <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:paddingBottomTextNormal}}>{dateModify}</Text>
                </View>

                <NeuButton onPress={()=>setAlert(true)} color={theme.colors.bgCol} height={width*0.12} width={width*0.12} borderRadius={width*0.09}>
                    <Ionicons name={"search-sharp"} size={width*0.07} color='#000' style={{marginRight: -5}} />
                </NeuButton>
            </View>


             {/* SECTION 1 */}
            <View style={{alignSelf: 'center', marginTop:paddingBottomBigGap,width:width*0.9}}>
             <NeuView color={theme.colors.bgCol} height={height*0.23} width={width*0.9} borderRadius={16}>
                
                <View>
                    <View style={{flex:1,flexDirection:'row',alignItems:'center',borderBottomWidth:2,borderColor:'#ddd', width:width*0.9,alignItems:'center',justifyContent:'space-between',paddingHorizontal:width*0.04}}>
                        <View>
                            <NeuView color='#F2F8FE' height={height*0.1} width={height*0.1} borderRadius={height*0.06} inset>

                                {weatherInfos.feather ? 
                                    <Feather size={height*0.09} color={ theme.colors.themeCol1 } name={weatherInfos.icon} />:
                                    weatherInfos.icon==='false' ?
                                    <Image style={{width:height*0.09,height:height*0.09,tintColor: weatherInfos.tintColor}} source={{uri: iconUrl}} />:
                                    weatherInfos.icon === 'image' ?
                                    <Image style={{height:height*0.09,width:height*0.09}} source={weatherInfos.source} /> 
                                    : <Ionicons color={ weatherInfos.tintColor} size={height*0.06} name={weatherInfos.icon} />
                                }  
                                  
                            </NeuView>

                        </View>


                        <View style={{flexDirection:'column',alignItems:'center'}}>
  
                            <Text style={{fontSize:width*0.1,letterSpacing:7,color:'#020202'}}>{temp.temp}º</Text>
                            
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <Ionicons color='#DA6574' name ="arrow-up-outline" size={width*0.05} />
                                    <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:paddingBottomTextNormal,paddingRight:7,paddingLeft:1}}>{temp.max}º</Text>
                                </View>
                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <Ionicons color="#3883A6" name ="arrow-down-outline" size={width*0.05} />
                                    <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:paddingBottomTextNormal,paddingLeft:1}}>{temp.min}º</Text>
                                </View>

                            </View>


                        </View>

                    </View>
                
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingHorizontal:width*0.04}}>
                        <View style={{flexDirection:'column',alignItems:'stretch',paddingVertical:'3%',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <Feather color="#969B9F" name="wind" size={width*0.05} />
                                <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:5,paddingLeft:10}}>{chosedWeather.wind_speed} Metre | Sec</Text>
                            </View>
                            <View style={{flexDirection:'row',alignItems:'center',paddingVertical:'3%'}}>
                                <Ionicons color="#969B9F" name="umbrella-outline" size={width*0.05} />
                                <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:5,paddingLeft:10}}>{chosedWeather.humidity}%</Text>
                            </View>

                        </View>
                        <View>
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <Feather color="#969B9F" name="sunrise" size={width*0.05} />
                                <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:5,paddingLeft:10}}>{moment.unix(chosedWeather.sunrise).format('HH:mm:ss')}</Text>
                            </View>
                            <View style={{flexDirection:'row',alignItems:'center',paddingVertical:'3%'}}>
                                <Feather color="#969B9F" name="sunset" size={width*0.05} />
                                <Text style={{color:'#969B9F',fontSize:fontsizeNormal,paddingTop:5,paddingLeft:10}}>{moment.unix(chosedWeather.sunset).format('HH:mm:ss')}</Text>
                            </View>

                        </View>

                        
                    </View>

                </View>
                
            </NeuView>
            </View>
            
            {/* SECTION 2 -Hourly cast */}
            <View style={{ borderBottomWidth:2, borderColor:'#ddd',paddingBottom:10}}>
                <Text style={{fontWeight:'500',fontSize:fontsizeNormal*1.1,marginBottom:width*0.02,marginTop:width*0.05}}>Hourly Forecast</Text>
                <View>
                <FlatList
                horizontal
                data={forecastHourly}
                renderItem={({item, index}) => {
                    const icon = forecast.hourly[index].weather[0].icon
                    const iconUrl = `https://openweathermap.org/img/wn/${icon}@4x.png`
                    return (
                    <View style={{paddingHorizontal:width*0.028,paddingVertical:width*0.028,alignSelf:'center',alignContent:'center',alignItems:'center'}}>
                        <TouchableOpacity
                        onPress={() => {
                            onChangePress(item,index)
                        }}>  
                        {item.style ?
                        <NeuView height={height*0.15} width={width*0.3} color='#FFFFFF' borderRadius={16} >  
                            <View style={{flexDirection:'column',alignItems:'center'}}>
                                <Text style={{color:'#A2A2A2',fontSize:fontsizeNormal}}>{index===0 ? 'Now': '1H Later'}</Text>
                                <Image style={{width:width*0.17,height:width*0.17,tintColor: "#389ACE"}} source={{uri: iconUrl}} />
                                <Text style={{fontSize:fontsizeMedium,letterSpacing:2}}>{Math.round(item.temp)}º</Text>               
                            </View>
                        </NeuView>
                        :
                        <NeuView height={height*0.15} width={width*0.3} color='#EBEBEB' borderRadius={16} inset>  
                            <View style={{flexDirection:'column',alignItems:'center'}}>
                                <Text style={{color:'#A2A2A2',fontSize:fontsizeNormal}}>{index===0 ? 'Now': '1H Later'}</Text>
                                <Image style={{width:width*0.17,height:width*0.17,tintColor: "#389ACE"}} source={{uri: iconUrl}} />
                                <Text style={{fontSize:fontsizeMedium,letterSpacing:2}}>{Math.round(forecast.hourly[index].temp)}º</Text>               
                            </View>
                            </NeuView>
                        } 
                                        
                        </TouchableOpacity>
                    </View>
                    
                    );
                }}
                keyExtractor={(item,index) => `${index}*100`} 
                >
                </FlatList>
                    
                </View>

            </View>

            {/* SECTION 3 -Daily cast */}
            <View style={{marginBottom: width*0.05}}>
                <Text style={{fontWeight:'500',fontSize:fontsizeNormal*1.1,marginBottom:width*0.02,marginTop:width*0.04}}>Daily Forecast</Text>
                <View>
                <FlatList
                horizontal
                data={forecastDaily}
                renderItem={({item, index}) => {
                    const icon = forecast.daily[index].weather[0].icon
                    const iconUrl = `https://openweathermap.org/img/wn/${icon}@4x.png`
                    return (
                    <View style={{paddingHorizontal:width*0.028,paddingVertical:width*0.028,alignSelf:'center',alignContent:'center',alignItems:'center'}}>
                        <TouchableOpacity
                        onPress={() => {
                            onChangePressDaily(item,index)
                        }}>  
                        {item.style ?
                        <NeuView height={height*0.15} width={width*0.3} color='#FFFFFF' borderRadius={16} >  
                            <View style={{flexDirection:'column',alignItems:'center'}}>
                                <Text style={{color:'#A2A2A2',fontSize:fontsizeNormal}}>{index===0 ? 'Today': 'Next day'}</Text>
                                <Image style={{width:width*0.17,height:width*0.17,tintColor: "#389ACE"}} source={{uri: iconUrl}} />
                                <Text style={{fontSize:fontsizeMedium,letterSpacing:2}}>{Math.round(item.temp.day)}º</Text>               
                            </View>
                        </NeuView>
                        :
                        <NeuView height={height*0.15} width={width*0.3} color='#EBEBEB' borderRadius={16} inset>  
                            <View style={{flexDirection:'column',alignItems:'center'}}>
                                <Text style={{color:'#A2A2A2',fontSize:fontsizeNormal}}>{index===0 ? 'Tomorrow': 'Next day'}</Text>
                                <Image style={{width:width*0.17,height:width*0.17,tintColor: "#389ACE"}} source={{uri: iconUrl}} />
                                <Text style={{fontSize:fontsizeMedium,letterSpacing:2}}>{Math.round(item.temp.day)}º</Text>               
                            </View>
                            </NeuView>
                        } 
                                        
                        </TouchableOpacity>
                    </View>
                    
                    );
                }}
                keyExtractor={(item,index) => `${index}*100`} 
                >
                </FlatList>
                    
                </View>

            </View>


            {alert && <ForecastModal width={width} height={height} forecast={forecast} receiveData={receiveData}/>}
           

            
        </View>
    </SafeAreaView>
  );
}
}

const weatherConditions = [
    {
        main:'Thunderstorm',
        descrip1:'There is thunder-storm',
        descrip2:'thunder-storm',
        descrip3:'today.',
        advise1:'Stay safe at home with a warm coffee could be the best!',
        advise2:'Take care of yourself!',
        images:require('../../photos/stormy.png'),
        feather: false,
        icon:"thunderstorm-outline",
        tintColor:'#B7ACEE'
    },
    {
        main:'Drizzle',
        descrip1:'There is a',
        descrip2:'drizzle',
        descrip3:'today.',
        advise1:"Don't forget to bring an umbrella or rain coat",
        advise2:'Take care of yourself!',
        images:require('../../photos/RainyDay2.png'),
        feather: true,
        icon:"cloud-drizzle",
        tintColor:'#B7ACEE'

    },
    {
        main:'Rain',
        descrip1:'Today is a',
        descrip2:'rainy',
        descrip3:'day.',
        advise1:"Don't forget to bring an umbrella or rain coat",
        advise2:'Take care of yourself!',
        images:require('../../photos/rainy.png'),
        feather: false,
        icon:"rainy-outline",
        tintColor:'#B7ACEE'
    },
    {
        main:'Snow',
        descrip1:'Today is a',
        descrip2:'snowy ',
        descrip3:'day.',
        advise1:"Keep your body warm from cold weather",
        advise2:'Take care of yourself!',
        images:require('../../photos/RainyDay.png'),
        feather: false,
        icon:"snow-outline"
    },
    {
        main:'Atmosphere',
        descrip1:'There is',
        advise1:"Stay alerted!",
        advise2:'Take care of yourself!',
        images:require('../../photos/rainy.png'),
        feather: false,
        icon:'false'
    },
    {
        main:'Clear',
        descrip1:'It is an amazing',
        descrip2:'sunny',
        descrip3:'day',
        advise1:"Going on a picnic or to the beach could be the best!",
        advise2:'Enjoy your day!',
        images:require('../../photos/sunnyBeachAdvise.png'),
        feather: false,
        icon:'image',
        source:require('../../photos/sunGIF.gif'),
        tintColor:'#ECD166'
    },
    {
        main:'Clouds',
        descrip1:'Today is a',
        descrip2:'cloudy',
        descrip3:'day.',
        advise1:"Joining outdoor activities with friends could be fun!",
        advise2:'Stay tunned!',
        images:require('../../photos/outdoor2.png'),
        feather: false,
        icon:"cloudy-outline",
        tintColor:'#389ACE'
    }
]

