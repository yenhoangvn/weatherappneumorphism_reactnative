import React from "react";
import { StyleSheet, Text, View } from "react-native";
// NEUMORPHISM-ELEMENTS
import { NeuView } from 'react-native-neu-element'

// NAVIGATION BOTTOM BAR
import { NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// ICONS
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

// VIEWS-COMPONENTS
import Home from './views/HomePage/Home';
import AirQuality from "./views/AirQuality/AirQuality";
import Forecast from './views/ForeCast/Forecast';

// RECOIL
import { RecoilRoot } from 'recoil'

import useScreenDimensions from "./hooks/useScreenDimensions";

const Tab = createBottomTabNavigator();


export default function App(){

  const screenDimension = useScreenDimensions()
  const width= screenDimension.width;
  const height = screenDimension.height;

   return(
    <RecoilRoot>
      <React.Suspense fallback={<Text>Loading....</Text>}>
        <NavigationContainer>
          <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Home') {
                  if(iconName = focused){
                    return(
                      <View style={{justifyContent:'center', alignItems:'center',position:'absolute',top:0}}>
                        <NeuView  color='#F1F0F5' height={height*0.067} width={width*0.25} borderRadius={width*0.05}>
                          <View style={{paddingBottom:5}}>
                            <Ionicons name='ios-home-outline' color="#D7872A" size={width*0.065}/>
                          </View>
                        </NeuView>

                      </View>
                    )
                  } else {
                    return(
                      <View style={{justifyContent:'center', alignItems:'center',position:'absolute',top:height*0.035/2}}>
                        <View style={{height:height*0.067,width:width*0.25,paddingBottom:5}}>
                          <Text style={{alignSelf:'center',color:'#797E85',fontWeight:'bold',fontSize:height * 0.021}}>Home</Text>
                        </View>
                      </View>
                   )}
              } else if (route.name === 'Forecast') {
                  
                  if(iconName = focused){
                    return(
                      <View style={{justifyContent:'center', alignItems:'center',position:'absolute',top:0}}>
                        <NeuView  color='#F1F0F5' height={height*0.067} width={width*0.25} borderRadius={width*0.05}>
                          <View style={{paddingBottom:5}}>
                          <Feather name='radio' color="#D7872A" size={width*0.065}/>
                          </View>
                        </NeuView>
                      </View>
                    )
                  }else{
                    return(
                      <View style={{justifyContent:'center', alignItems:'center',position:'absolute',top:height*0.035/2}}>
                        <View style={{height:height*0.067,width:width*0.25,paddingBottom:5}}>
                          <Text style={{alignSelf:'center',color:'#797E85',fontWeight:'bold',fontSize:height * 0.021}}>Forecast</Text>
                        </View>
                      </View>
                    )}
              }else if(route.name === 'AirQuality'){ 
              
                if(iconName = focused){
                  return(
                    <View style={{justifyContent:'center', alignItems:'center',position:'absolute',top:0}}>
                      <NeuView  color='#F1F0F5' height={height*0.067} width={width*0.25} borderRadius={width*0.05}>
                        <View style={{paddingBottom:5}}>
                        <Feather name='wind' color="#D7872A" size={width*0.065}/>
                        </View>
                      </NeuView>
                    </View>
                  )
                }else{
                  return(
                    <View style={{justifyContent:'center', alignItems:'center',position:'absolute',top:height*0.035/2}}>
                        <View style={{height:height*0.067,width:width*0.25,paddingBottom:5}}>
                          <Text style={{alignSelf:'center',color:'#797E85',fontWeight:'bold',fontSize:height * 0.021}}>Air Quality</Text>
                        </View>
                    </View>
                  )}
              }
            },
          })}

          tabBarOptions={{
            showLabel:false,
            style: {
              position:'absolute',
              left:width*0.05,
              // right:width*0.05,
              bottom:width*0.035,
              // backgroundColor:'#F0F0F4',
              borderRadius:height*0.07,
              backgroundColor: '#e6e5eb',
              borderColor: '#F4F6FB',
              borderWidth: 6,
              overflow: 'hidden',
              shadowColor: '#000',
              shadowRadius: 6,
              shadowOffset:{
                height:6,
                width:6
              },
              shadowOpacity: 1,
              height:height*0.07,
              width:width*0.91,
              alignItems:'center',
              justifyContent:'space-around'
            }
          }}
          >
            <Tab.Screen name="Home" component={Home}
            />
            <Tab.Screen name="Forecast" component={Forecast}
            // options={{ tabBarBadge: 3 }}
            />
            <Tab.Screen name="AirQuality" component={AirQuality} />
            
          </Tab.Navigator>
        </NavigationContainer>
       </React.Suspense>
    </RecoilRoot>
 )
}
 const styles = StyleSheet.create({
   container: {
     marginTop: 25,
     padding: 10
   }
 });
 
