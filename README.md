## NEUMORPHISM WEATHER MOBILE APP

### DESCRIPTION
Weather app developed in React Native with Expo, using AsyncStorage to persist data. It allows you to get all weather informations( include current weather and daily forecast weather ) base on your current location. Moreover, you can search for the weather of different places.

----

### API SOURCE

OpenWeatherMap - <http://openweathermap.org/api> 

Google Place Autocomplete/Places API - <https://developers.google.com/places/web-service/autocomplete>

----

### HOW TO RUN IT
#### From client folder
```
1. cd client
2. npm install
3. npm start
```
#### From server folder
```
1. cd server
2. npm install
3. nodemon
```

