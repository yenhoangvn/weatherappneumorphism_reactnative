
import { atom, AtomEffect,DefaultValue } from 'recoil'
import AsyncStorage from '@react-native-async-storage/async-storage'

const persistAtom = ({ node, setSelf, onSet }) => {
    setSelf(
        AsyncStorage.getItem(node.key).then((savedValue) =>
            savedValue != null ? JSON.parse(savedValue) : new DefaultValue(),
        ),
    )

    onSet((newValue) => {
        if (newValue instanceof DefaultValue) {
            AsyncStorage.removeItem(node.key)
        } else {
            AsyncStorage.setItem(node.key, JSON.stringify(newValue))
        }
    })
}


export const recentViewedCitiesState = atom({
    key: 'citiesSavedState',
    default: [],
    effects_UNSTABLE: [persistAtom],
  });